#ifndef STATUS_HPP
#define STATUS_HPP

#include <stdint.h>
#include <protobuf.hpp>

class status {
public:
  enum class modulation_mode {
    FSK = 0,
    LORA
  };

  static status&
  instance()
  {
    static status inst;
    return inst;
  }

  void
  set_from_proto(const ground_station_status<20> &params);

  void
  update_datetime();

  bool
  in_rx() const;

private:
  status();

  char d_current_datetime[20] = "";
  bool d_rx;
  modulation_mode d_mode;
  float d_rx_freq;
  uint64_t d_frames_cnt = 0;
};

#endif // STATUS_HPP
