#include <EEPROM.h>
#include <config.hpp>

#define EEPROM_SIZE 4096

config::config() :
  IotWebConf(AP_NAME, &dns_server, &web_server, INITIAL_AP_PASS, CONFIG_VERSION),
  web_server(80)
{
  web_server.on("/", [this] { handle_root(); });
  web_server.on("/config", [this] { handleConfig(); });
  web_server.on("/reset", [this] { config_reset(); });
  web_server.onNotFound([this]{ handleNotFound(); });

  setConfigSavedCallback([this] { config_saved(); });
  setFormValidator([this] (iotwebconf::WebRequestWrapper *webRequestWrapper) -> bool { return form_validator(webRequestWrapper); });
  getApTimeoutParameter()->visible = true;
  getThingNameParameter()->label = "Ground Station Name (will be seen on the map)";
  getApPasswordParameter()->label = "Password for this dashboard (user is <b>admin</b>)";
  
  station_group.addItem(&station_id_param);
  station_group.addItem(&api_key_param);
  station_group.addItem(&lat_param);
  station_group.addItem(&lon_param);
  addParameterGroup(&station_group);
  
  mqtt_group.addItem(&mqtt_server_param);
  mqtt_group.addItem(&mqtt_port_param);
  mqtt_group.addItem(&mqtt_user_name_param);
  mqtt_group.addItem(&mqtt_pass_param);
  addParameterGroup(&mqtt_group);
}

void
config::handle_root()
{
  if (handleCaptivePortal())
  {
    return;
  }
  String s = "<!DOCTYPE html><html lang=\"en\"><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\"/>";
  s += "<title>SatNOGS tiny</title></head><body>";
  s += "Go to <a href='config'>configure page</a> to change settings.<br>";
  s += "<a href='reset'>Reset</a>";
  s += "</body></html>\n";

  web_server.send(200, "text/html", s);
}

void
config::config_saved()
{
  need_reset = true;
}

void
config::config_reset()
{
  Serial.println("Reset config... ");
  EEPROM.begin(EEPROM_SIZE);
  
  for (int i = 0 ; i < EEPROM_SIZE ; i++) {
    EEPROM.write(i, 0);
  }

  EEPROM.commit();
  need_reset = true;
  Serial.println("Success!");
}

bool
config::form_validator(iotwebconf::WebRequestWrapper *webRequestWrapper)
{
  return true;
}

String
config::get_mqtt_server() const
{
  return String(mqtt_server);
}

int
config::get_mqtt_port() const
{
  return atoi(mqtt_port);
}

String
config::get_mqtt_username() const
{
  return String(mqtt_username);
}

String
config::get_mqtt_pass() const
{
  return String(mqtt_pass);
}

int
config::get_station_id() const
{
  return atoi(station_id);
}

float
config::get_station_lat() const
{
  return atof(lat);
}

float
config::get_station_lon() const
{
  return atof(lon);
}

void
config::mqtt_clear_credentials()
{
  mqtt_username[0] = '\0';
  mqtt_pass[0] = '\0';
}

void
config::restart_if_needed()
{
  if (need_reset) {
    Serial.println("Rebooting after 1 second.");
    need_reset = false;
    delay(1000);
    ESP.restart();
  }
}
