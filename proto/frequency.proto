syntax = "proto3";

/*
 * A pair with a time offset and the corresponding frequency.
 * Used to specify frequency variations at a time relative to a reference,
 * due to the Doppler effect
 */
message doppler_offset
{
  uint64 millis = 1; // Offset from a time reference in milliseconds
  float freq = 2;    // The frequency for the given time instant
}

/*
 * Just a wrapper to support oneof
 */
message doppler_array {
  repeated doppler_offset offsets = 1;
}

/*
 * Doppler polynomials for curve fitting
 */
message doppler_poly
{
  float A = 1;
  float B = 2;
  float C = 3;
}

/*
 * Specifies the frequency and how it varies depending on the Doppler.
 * If there is the need for momentarily frequency setting, use the array with
 * as single element at offset 0
 */
message frequency {
  string reference =	1; // The time reference for the rest of the message in ISO8601
  oneof method {
    doppler_array array = 2;
    doppler_poly poly = 3;
  }
}
